# coding:utf-8

from selenium import webdriver
import os
import traceback
import time
import pandas as pd
import logging
import argparse
import requests
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from retry import retry
from bs4 import BeautifulSoup
from tqdm import tqdm

# result
TEN_GIGA = '10ギガ'
FIVE_GIGA = '5ギガ'
ONE_GIGA = 'お申し込み受付中'

# kokudo_chiriin.csv
CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/kokudo_chiriin.csv'
# CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/test.csv'

# selenium wait sec
WAIT_TIME = 3

# selenium timeout exception sec
EXCEPT_TIME = 4

# OUTPUT
RESULT_CSV = 'result_gfast.csv'

INPUT_ZIP_NUM = '郵便番号'
SYSTEM_ADDRESS = '住所'
MANSION_NAME = 'マンション名'
SERVICE_AREA_NAME = '提供サービス名'

# columns
columns = [INPUT_ZIP_NUM, SYSTEM_ADDRESS, MANSION_NAME, SERVICE_AREA_NAME]

############################################################
#
# 前処理
#
############################################################
logger = logging.getLogger(__name__)
fmt = "%(asctime)s %(levelname)s %(name)s :%(message)s"
logging.basicConfig(level=logging.INFO, filename='au_hikari.log', format=fmt)

# コマンドライン実行引数処理
parser = argparse.ArgumentParser()
parser.add_argument(
    "--debug", help="--debug is browser mode option", action="store_true")
parser.add_argument("--limit", help="--limit is define exec number")
parser.add_argument("--offset", help="--offset is define start number")
parser.add_argument("--mode", help="--mode is add or new")
parser.add_argument("--file", help="--file is output csv_file_name ")
parser.add_argument("--csv", help="--csv is input csv_file_name ")
parser.add_argument(
    "--ss", help="--ss is capture screenshot option", action="store_true")
args = parser.parse_args()

# file_nameの指定がない場合
if args.file is not None:
    RESULT_CSV = os.path.dirname(os.path.abspath(
        __file__)) + '/' + args.file
else:
    RESULT_CSV = os.path.dirname(os.path.abspath(
        __file__)) + '/result_gfast.csv'

# chromedriver
DRIVER_PATH = os.path.dirname(os.path.abspath(__file__)) + '/chromedriver'

if args.debug:
    driver = webdriver.Chrome(executable_path=DRIVER_PATH)
else:
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Chrome(
        executable_path=DRIVER_PATH, chrome_options=options)

# テスト用csvファイルを指定する為のオプション
if args.csv is not None:
    CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/' + args.csv
else:
    CSV_PATH

# クロール対象
WEB_SITE = 'https://bb-application.au.kddi.com/auhikari/zipcode'

# select first elements
SELECT_ELEMENT = '//*[@id="selection-block"]/table/tbody/tr[1]/td[1]/a'

# 繰り返し画面遷移用
EXCEPT_SELECT_ELEMENT_CLASS_NAME = 'heightLine-areaptA'

# auひかりホームS 判定用
IS_AU_HIKARI_TYPE = '//*[@id="contents"]/div/form/div[4]/div/ul/li[2]'

# お申し込み受付中 判定用
IS_SERVICE = '//*[@id="contents"]/div/form/div[4]/div/ul/li[1]/p'

# 号・番地を入力 判定用
NEED_DETAIL = '//*[@id="contents"]/div/form/div[1]/div/p'

# エリア詳細判定用
RESULT_DETAIL_AREA = '// *[ @ id = "address-box"] / div / ul / li[1] / div / p[2]'


############################################################
#
# scrennshot取得
#
############################################################
def screenshot_page(_driver,_zipcode):

    screenshot_filename = os.path.dirname(os.path.abspath(__file__)) + '/screenshots/' + _zipcode + '.png'
    _driver.save_screenshot(screenshot_filename)


############################################################
#
# 全国郵便番号取得
#
############################################################
def load_zipcode_csv():
    zip_list = []
    zipcodes = pd.read_csv(
        CSV_PATH, usecols=[1], encoding="shift_jis")
    zipcodes_unique = zipcodes.drop_duplicates()
    print(' Csv count : {0}'.format(len(zipcodes_unique)))
    for index, row in zipcodes_unique.iterrows():
        zipcode = row.values
        zip_list.append(zipcode)
    return zip_list


############################################################
#
# util.csv初期化処理(Header定義)
#
############################################################
def init_csv():
    df = pd.DataFrame(columns=columns)
    df.to_csv(RESULT_CSV, mode='w', header=True,
              index=False, encoding='utf-8')


############################################################
#
# util.csv書き込み
#
############################################################
def add_row_to_csv(write_list):

    if len(write_list) != len(columns):
        not_enough_columns = len(columns) - len(write_list)

        not_enough_list = [' '] * not_enough_columns
        write_list = write_list + not_enough_list

        df = pd.DataFrame([write_list], columns=columns)

    else:

        df = pd.DataFrame([write_list], columns=columns)

    logger.info(' 結果 : {0} '.format(write_list))
    df.to_csv(RESULT_CSV, mode='a', header=False,
              index=False, encoding='utf-8')


############################################################
#
# 繰り返し利用
#
############################################################
def search_service_area(driver, in_zip1, in_zip2):

    driver.get(WEB_SITE)

    # 1page
    # click mantion type
    driver.find_element_by_id('mantion').click()

    # zip1
    zip1 = driver.find_element_by_id('sendzip1')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    zip1.clear()
    zip1.send_keys(in_zip1)

    # zip2
    zip2 = driver.find_element_by_id('sendzip2')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    zip2.clear()
    zip2.send_keys(in_zip2)

    # search
    search_button = WebDriverWait(driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, '//*[@id="BtnZip"]/span/a/span/span'))
    )
    search_button.click()


############################################################
#
# マンションを網羅する必要があるかの判定関数
#
############################################################
def need_mansion_discovery():

    NOT_FOUND_XPATH = '//*[@id="contents"]/div/div[3]/div/p'
    not_found_element = driver.find_element_by_xpath(NOT_FOUND_XPATH)

    print('判定文字列：', not_found_element.text)

    if 'マンションがありません' in not_found_element.text:
        print('マンションがみつかりませんでした')
        logger.info(' need_mansion_discovery : マンションが見つかりませんでした')
        return False

    else:
        print('マンションが見つかりました。')
        logger.info(' need_mansion_discovery : マンションが見つかりました')
        return True


############################################################
#
# マンションを網羅検索
#
############################################################
def mansion_discovery():

    MANSION_CSS_SELECTOR = '#aparts-box > div.MTblArea > table > tbody > tr'
    mansion_elements = driver.find_elements_by_css_selector(MANSION_CSS_SELECTOR)

    print('住所あたりのマンション件数：', len(mansion_elements))
    logger.info('住所あたりのマンション件数 : {0}'.format(len(mansion_elements)))

    i = 0
    for n in range(len(mansion_elements)):
        print('マンションごとの解析開始')
        if i == 0:
            print('マンション１件めの解析開始')
            logger.info('マンション名 : {0}'.format(mansion_elements[n].text))

            mansion_elements[n].click()
            time.sleep(WAIT_TIME)
            driver.find_element_by_xpath('//*[@id="RightBtn"]/span/span').click()
            time.sleep(WAIT_TIME)
            judge_service_area()
        else:
            print('マンションごとの解析開始')
            print('ブラウザバックします。')
            driver.back()
            time.sleep(WAIT_TIME)
            mansion_elements = driver.find_elements_by_css_selector(MANSION_CSS_SELECTOR)
            time.sleep(WAIT_TIME)

            logger.info('マンション名 : {0}'.format(mansion_elements[n].text))
            mansion_elements[n].click()
            time.sleep(WAIT_TIME)

            driver.find_element_by_xpath('//*[@id="RightBtn"]/span/span').click()
            time.sleep(WAIT_TIME)

            judge_service_area()

        i += 1


############################################################
#
# 結果判定
#
############################################################
def judge_service_area():

    result_list = []
    SERVICE_AREA_SELECTOR = '#address-box > div > ul > li.ResArea > div > p'
    service_area_elements = driver.find_elements_by_css_selector(SERVICE_AREA_SELECTOR)

    for service_area_element in service_area_elements:

        result_service_type_lf = service_area_element.text
        result_service_type = result_service_type_lf.strip()

        logger.info('結果判定住所 : {0}'.format(result_service_type))
        result_list.append(result_service_type)

    try:
        # judge_element = WebDriverWait(driver, EXCEPT_TIME).until(
        #     EC.presence_of_element_located(
        #         (By.CLASS_NAME, 'serviceContBox'))
        # )
        judge_element = WebDriverWait(driver, EXCEPT_TIME).until(
            EC.presence_of_element_located(
                (By.XPATH, '//*[@id="contents"]/div/div[4]/div[1]/ul[2]/li/p/span[2]'))
        )

    except NoSuchElementException:
        judge_element = WebDriverWait(driver, EXCEPT_TIME).until(
            EC.presence_of_element_located(
                (By.CLASS_NAME, 'IntitL fwb fs24'))
        )

    service_type = judge_element.text.strip()

    result_list.append(service_type)

    add_row_to_csv(result_list)


############################################################
#
# auひかり自動入力関数
#
############################################################
@retry(delay=10, backoff=1, max_delay=80)
def au_hikari(in_zip1, in_zip2):

    driver.get(WEB_SITE)

    search_service_area(driver, in_zip1, in_zip2)

    result_service_aria = []

    result_zip_code = str(in_zip1) + '-' + str(in_zip2)

    # NoSuchElementException / TimeoutException捕捉のためのtry
    try:

        # 初回
        element_next_pages = driver.find_elements_by_class_name(
            EXCEPT_SELECT_ELEMENT_CLASS_NAME)

        time.sleep(WAIT_TIME)
        print('住所 {0} におけるクリック可能件数 {1} '.format(result_zip_code, len(element_next_pages)))
        logger.info('住所 {0} におけるクリック可能件数 {1} '.format(result_zip_code, len(element_next_pages)))

        # 住所あたりのクリック可能な丁目番地情報が存在しない場合
        if len(element_next_pages) == 0:

            if need_mansion_discovery():
                mansion_discovery()

            else:
                area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, RESULT_DETAIL_AREA))
                )

                result_service_aria = [result_zip_code, area_name_element.text, '', 'マンションが見つかりませんでした']
                add_row_to_csv(result_service_aria)

        # 住所あたりのクリック可能な丁目番地情報が複数存在する場合
        else:
            i = 0
            for n in range(len(element_next_pages)):

                if i == 0:
                    element_next_pages[n].click()
                    time.sleep(WAIT_TIME)
                    if need_mansion_discovery():
                        mansion_discovery()

                    else:
                        area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                            EC.presence_of_element_located(
                                (By.XPATH, RESULT_DETAIL_AREA))
                        )

                        result_service_aria = [result_zip_code, area_name_element.text, '', 'マンションが見つかりませんでした']
                        add_row_to_csv(result_service_aria)

                else:
                    search_service_area(driver, in_zip1, in_zip2)
                    time.sleep(WAIT_TIME)
                    element_next_pages = driver.find_elements_by_class_name(
                        EXCEPT_SELECT_ELEMENT_CLASS_NAME)

                    time.sleep(WAIT_TIME)
                    element_next_pages[n].click()
                    time.sleep(WAIT_TIME)

                    child_element_next_pages = driver.find_elements_by_class_name(
                        EXCEPT_SELECT_ELEMENT_CLASS_NAME)

                    if len(child_element_next_pages) == 0:
                        if need_mansion_discovery():
                            # print('マンションを検索します')
                            mansion_discovery()

                        else:
                            # print('マンションの検索は不要です')
                            area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                                EC.presence_of_element_located(
                                    (By.XPATH, RESULT_DETAIL_AREA))
                            )

                            result_service_aria = [result_zip_code, area_name_element.text, '', 'マンションが見つかりませんでした']
                            add_row_to_csv(result_service_aria)

                    else:
                        j = 0
                        for m in range(len(child_element_next_pages)):

                            # print('j loop', str(j))

                            if j == 0:

                                child_element_next_pages[m].click()
                                time.sleep(WAIT_TIME)

                                if need_mansion_discovery():
                                    # print('マンションを検索します')
                                    mansion_discovery()

                                else:
                                    # print('マンションの検索は不要です')
                                    area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                                        EC.presence_of_element_located(
                                            (By.XPATH, RESULT_DETAIL_AREA))
                                    )

                                    result_service_aria = [result_zip_code, area_name_element.text, '', 'マンションが見つかりませんでした']
                                    add_row_to_csv(result_service_aria)

                            else:
                                search_service_area(driver, in_zip1, in_zip2)
                                time.sleep(WAIT_TIME)

                                element_next_pages = driver.find_elements_by_class_name(
                                    EXCEPT_SELECT_ELEMENT_CLASS_NAME)

                                time.sleep(WAIT_TIME)

                                element_next_pages[i].click()

                                time.sleep(WAIT_TIME)
                                child_element_next_pages = driver.find_elements_by_class_name(
                                    EXCEPT_SELECT_ELEMENT_CLASS_NAME)

                                time.sleep(WAIT_TIME)
                                child_element_next_pages[m].click()
                                time.sleep(WAIT_TIME)

                                if need_mansion_discovery():
                                    # print('マンションを検索します')
                                    mansion_discovery()

                                else:
                                    # print('マンションの検索は不要です')
                                    area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                                        EC.presence_of_element_located(
                                            (By.XPATH, RESULT_DETAIL_AREA))
                                    )

                                    result_service_aria = [result_zip_code, area_name_element.text, '', 'マンションが見つかりませんでした']
                                    add_row_to_csv(result_service_aria)

                            j += 1

                i += 1


    # 存在しないZIPCode もしくは 電話番号入力が必要 もしくは 提供エリアではない
    except TimeoutException:
        try:
            time.sleep(WAIT_TIME)
            need_detail_info_element = WebDriverWait(driver, EXCEPT_TIME).until(
                EC.presence_of_element_located(
                    (By.XPATH, '//*[@id="contents"]/div/p[1]'))
            )

            need_detail_info_message = need_detail_info_element.text

            area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                EC.presence_of_element_located(
                    (By.XPATH, '//*[@id="address-box"]/div/ul/li[1]/div/p[2]'))
            )

            result_area_name = area_name_element.text

            if '提供エリアを詳細に確認するため' in need_detail_info_message:

                need_address_or_phone_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="contents"]/div/form/div[1]/div/p'))
                )

                need_address_or_phone_text = need_address_or_phone_element.text

                if '番地' in need_address_or_phone_text:
                    input_address(driver,result_zip_code,result_service_aria)


                if '電話番号' in need_address_or_phone_text:

                    result_service_aria = [
                        result_zip_code, result_area_name, False, False]

                    add_row_to_csv(result_service_aria)

            else:
                result_area_name = 'エリア名が取得出来ません。'
                """result_area_name = 'ERR0001:想定外な状態です(電話番号が必要と判定されたが、文字列が含まれない)'"""
                result_service_aria = [result_zip_code,
                                       result_area_name, False, False]

                add_row_to_csv(result_service_aria)

        except WebDriverException:
            try:
                time.sleep(WAIT_TIME)
                error_message_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="contents"]/div/div[2]'))
                )
                error_message_text = error_message_element.text

                result_area_name = '郵便番号と合致する住所が見つかりませんでした'

                if '郵便番号と合致する住所が見つかりませんでした' in error_message_text:
                    result_service_aria = [result_zip_code,
                                           result_area_name, False, False]

                add_row_to_csv(result_service_aria)

                logger.info(' ZIP Code : {0} is {1}'.format(
                    result_zip_code, result_area_name))

            except NoSuchElementException:
                result_area_name = 'ERR0003:想定外な状態です(合致住所が見つからない：NoSuchElementException)'
                result_service_aria = [result_zip_code,
                                       result_area_name, False, False]

                add_row_to_csv(result_service_aria)

            except TimeoutException:
                result_area_name = 'ERR0004:想定外な状態です(合致住所が見つからない：TimeoutException)'
                result_service_aria = [result_zip_code,
                                       result_area_name, False, False]

                add_row_to_csv(result_service_aria)

    finally:
        if args.ss:
            screenshot_page(driver, result_zip_code)
        else:
            pass


############################################################
#
# au_hikari_list
#
############################################################
def au_hikari_list(list):
    for zip_code in tqdm(list):
        str_zip_code = str(zip_code[0])
        s_zip1 = str_zip_code[:3]
        s_zip2 = str_zip_code[3:]
        au_hikari(s_zip1, s_zip2)


############################################################
#
# main
#
############################################################
if __name__ == '__main__':
    try:

        list_zip = load_zipcode_csv()

        # limit
        _limit = int(args.limit) if args.limit else None

        if args.mode == 'new':
            init_csv()

        # offset
        if args.offset:
            # listの開始はゼロ開始のため減算する
            offset = args.offset
            _offset = int(offset)

            if _limit is None:
                _list_zip = list_zip[(_offset-1):]
            else:
                _list_zip = list_zip[(_offset-1):(_limit)]
        else:
            if _limit is None:
                _list_zip = list_zip
            else:
                _list_zip = list_zip[:(_limit)]

        logger.info('[Main] offset={0} ,list_size={1} ,limit={2}'.format((args.offset), len(_list_zip), (_limit)))

        au_hikari_list(_list_zip)

    # TimeoutException / NoSuchElementException以外を捕捉
    except:
        print(traceback.format_exc())
        logger.error(traceback.format_exc())

    finally:
        driver.close()
