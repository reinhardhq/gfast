#!/bin/bash

python index.py --limit 2000  &
sleep 1
python index.py --offset 2001 --limit 4000  &
sleep 1
python index.py --offset 4001 --limit 6000  &
sleep 1
python index.py --offset 6001 --limit 8000  &
sleep 1
python index.py --offset 8001 --limit 10000  &
sleep 1
python index.py --offset 10001 --limit 12000  &
sleep 1
python index.py --offset 12001  &
